MPSL Release Notes
==================

2.0.2
-----

 - New features:
    - The push() functions accepts a variable number of elements
      to be pushed into the array (not only one).
    - New function tr(), to transliterate strings.
    - New function int(), to return the integral part of a number.

2.0.1
-----

 - New features:
    - An abbreviated way of defining hashes (ala JavaScript).
 - Bugfixes:
    - Several fixes to for() construction: fixed incorrect
      calling order and for (;;) is accepted for infinite loops.

2.0.0
-----

 - A new C-like for() control flow construction.
 - Updated to match MPDM refactoring (no more sweeping /
   garbage collection).
 - New multithreading support:
    - A function can be prefixed with & (ampersand) to execute
      it in a new thread; the new thread handler is returned.
    - Support for mutex values.
    - Support for semaphore values.
 - Any function argument not explicitly declared and
   sent as argument in the function call is stored
   in the local array _ (underscore).
 - The adel() function no longer returns the deleted element.
 - mpsl_compile_file() now has an additional argument
   holding the source file search path.
 - The following functions had had their arguments swapped:
    - split().
    - join().
    - sscanf().
    - map().
    - grep().
    - regex().
    - sregex().

1.0.9
-----

 * New syntax for _inverse_ calling functions; the new
   construction `val1->func(val2)' is equivalent
   to `func(val1, val2)'. This way, chained function
   calls where the first argument of one is the return
   of the other are much easier to write and understand.
 * Numbers prefixed by `0b' are accepted as binary numbers
   and `sprintf()' accepts `%b' to print them.
 * New operands >> and << (bit shifting).
 * New operands &=, |= and ^=, <<= and >>=.
 * New operand ** (power).
 * New function `random()'.
 * New trapping functionality.

1.0.8
-----

 * New function `sscanf()'.
 * New function `compile()', to compile a string of MPSL code
   to an executable value.
 * New function `bincall()', to create an executable value
   given a pointer to binary code.
 * The `mpsl' executable now ignores the first line of an MPSL
   script if it starts with #!.
 * Real numbers can be expressed in scientific notation.

1.0.7
-----

 * The `write()' allows a variable argument of values to write.
 * Values are dumped as MPSL instead of MPDM's low-level format.
 * New function `chdir()'.

1.0.6
-----

 * Fixed a strange but annoying bug regarding local variables
   losing their values in anonymous subroutines.

1.0.5
-----

 * New `g' flag in regex().
 * Fix backslash parsing in strings.

1.0.4
-----

 * The iterator variable in foreach() is _always_ created
   as a local variable inside the block. Also, the old
   construction foreach(local v, ...) issues a warning,
   as it's redundant.

1.0.3
-----

 * Anonymous subroutines now run inside a block frame
   instead of a subroutine frame. This way, they can have
   access to local variables defined in the subroutine
   they are called from. So, for example, if you run
   map() inside a subroutine, its anonymous code block
   can access the local variables defined there.
 * getenv() doesn't crash if the environment variable
   does not exist.

1.0.2
-----

 * Small internal refactoring regarding subroutine and
   block frames. Frame creation code has been unified,
   so block frames don't crash when a NULL local symbol
   table is sent as argument, and can have arguments.
   This change is not visible from outside; only a step
   towards closing bug #1107.
